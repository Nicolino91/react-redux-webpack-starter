import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as nameActions from '../../actions/nameActions';

class App extends React.Component {
    constructor(props) {
        super(props);
        
        this.handleChange = this.handleChange.bind(this);
        this.changeName = this.changeName.bind(this);
    }

    componentWillMount(){
        this.setState(this.props);
    }
    changeName() {
        this.props.actions.newName(this.state.name);
    }

    handleChange(e){
        this.setState({name: e.target.value})
    }

    render() {
        return (
            <div>
                <h1>Hello, {this.props.name}</h1>
                <input
                    onChange={this.handleChange}
                    value={this.state.name}
                />
                <button onClick={this.changeName}>Change Name</button>
            </div>
        );
    }
}

App.propTypes = {
    name: PropTypes.string,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProprs) {
    return {
        name: state.name
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(nameActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);