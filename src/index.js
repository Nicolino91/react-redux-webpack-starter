import React from 'react';
import { render } from 'react-dom';
import configureStore from "./store/configureStore";

import App from "./components/App/app.component";
import { Provider } from 'react-redux';


const store = configureStore();

render(
    <Provider store={store}>
        <App name={store.name} />
    </Provider>
    , document.getElementById("app")
);