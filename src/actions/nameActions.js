import * as types from './actionTypes';

export function newName(name) {
    return {type: types.CHANGE_NAME , name}
}