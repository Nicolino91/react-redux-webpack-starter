import * as types from '../actions/actionTypes';

export default function nameReducer(state = "", action) {
    switch (action.type) {
        case types.CHANGE_NAME:
            return action.name;
        default:
            return "Pippo";
    }
}