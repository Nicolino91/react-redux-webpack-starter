import {combineReducers} from 'redux';
import name from "./nameReducer";

const rootReducer = combineReducers({name});

export default rootReducer;